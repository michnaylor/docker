# Bug-dedup base
FROM ubuntu:14.04

MAINTAINER Michelle Naylor mnaylor@ualberta.ca

RUN apt-get update && apt-get install -y \
    build-essential \
    python \
    python-dev \
    python-pip \
    git \
    uuid-dev \
    sqlite3 \
    libsqlite3-dev \
    libtool \
    automake \
    libboost-all-dev \
    cmake || echo "Ignoring poor return value"
# install java 
RUN apt-get install -y \
    openjdk-7-jdk \
    openjdk-7-jre 

#install perl and python
RUN apt-get install -y \
    cpanminus \
    r-base \
    littler \
    wget \
    python-numpy \
    python-scipy 

# Fix R LOCALE errors
ENV LANG "en_US.UTF-8"

# SSH key stuff
RUN mkdir /root/.ssh
ADD id_rsa /root/.ssh/id_rsa
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# install perl modules
RUN cpanm -f Heap::Simple::XS

# setup zeromq
RUN wget http://download.zeromq.org/zeromq-2.1.4.tar.gz
RUN tar -xzvf zeromq-2.1.4.tar.gz
RUN cd zeromq-2.1.4/ && ./configure && make && sudo make install && ldconfig

# setup mongrel2
RUN git clone https://github.com/zedshaw/mongrel2.git
RUN cd ~ && cd mongrel2 && make all install && \
    cd examples/python && python setup.py install

RUN pip install nltk pyzmq python-dateutil pyinotify sphinx

RUN mkdir logs

# setup vowpal_wabbit
RUN git clone https://github.com/JohnLangford/vowpal_wabbit.git && \
    cd vowpal_wabbit && \
    make -j 8 && make install

# setup flann
RUN git clone https://github.com/mariusmuja/flann.git && \
    cd flann && cmake ./ && make && make install

ENV LOGNAME docker
