Using Devenv
====================

The devenv script provides an easy-to-use wrapper that automates many
common docker tasks related to this repo. Before using it, you should
set the username and database variables near the top of the script.
There are a number of other variables you might want to change, all
appearing near the top of the script.

Once the script has been modified to your liking, you can run

		./devenv build

To build all of the docker images you need. (This takes a while)
Once the docker images have been built,

		./devenv up

will bring up the development environment.


		./devenv serve tests

will start the webserver in a docker container, connected to the other
containers started with devenv up (using the test config). This will run
front\_controller using util/dev\_server.py by default, so it will
be automatically restarted when you change a file.

		./devenv shell

will launch a shell in the docker container that is running the server, and

		./devenv run <some command>

will run commands in the container that is running the server.

Both the shell and run commands start in /src/bugparty, which is the mounted
copy of bugparty from your host machine. By default, this folder is
equivalent to ~/bugparty on your host, but you can change which folder is
mounted for development in devenv.


To run the server with the non-test config, you can run

		./devenv serve

Build Requirements
=====================
The dockerfiles required to deploy bug-deduplication and bugparty

your private ssh key (id\_rsa) should be copied to this directory


Build Containers
================
* couchdb container runs couchdb instance

        sudo docker build -rm -t <username>/couchdb:<tagname> couchdb/

* elasticsearch container runs elasticsearch instance

        sudo docker build -rm -t <username>/elasticsearch:<tagname> elasticsearch/

* base container upon which other containers are based. Must be built before all subsequent containers

        sudo docker build -rm -t mnaylor/base .

* couchdb bug-dedup container runs bugparty, bug-deduplication

        sudo docker build -rm -t <username>/bug-dedup:couch bug-dedup-couch/.

* elasticsearch bug-dedup container runs bugparty, bug-deduplication

        sudo docker build -rm -t <username>/bug-dedup:elasticsearch bug-dedup-es/.

* data container holds bug-deduplication and fastrep output

        sudo docker build -rm -t <username>/data:<tagname> data/.


Run Containers
==============

CouchDB
-------
1. run couchdb instance

        sudo docker run -d -p 5984:5984 -v ~/couchdb:/usr/local/var/lib/couchdb --name couchdb <username>/couchdb:<tagname>

2. run data volume

        sudo docker run -d -i --name data <username>/data:<tag> /bin/bash

3. run bug-dedup and link it to couchdb and data containers

        sudo docker run -t -p 8080:8080 --link couchdb:couchdb --volumes-from data <username>/bug-dedup:couch python front_controller.py --workdir=/bugparty

Elasticsearch
-------------
1. run elasticsearch instance

        sudo docker run -d -t -p 9200:9200 -p 9300:9300 --name es <username>/elasticsearch:<tagname> elasticsearch start

2. run data volume

        sudo docker run -d -i --name data <username>/data:<tag> /bin/bash

3. run bug-dedup and link it to couchdb and data containers
        sudo docker run -t -p 8080:8080 --link es:es --volumes-from data <username>/bug-dedup:elasticsearch python front_controller.py --workdir=/bugparty

Troubleshooting
---------------------

* Did the build die at add id_rsa ? 
  * Well run ssh-keygen and copy the id_rsa and id_rsa.pub you like and put it into the docker directory that you cloned. It will add those instead. Don't share any keys you don't want to share.
  * You should make a key pair before you start in your docker/ dir
     * ssh-keygen -t rsa -N "" -f ./id_rsa
  * Make that id_rsa.pub a bitbucket account just in case
* Did the git repos not clone properly from the devenv build?
  * go and change the docker files from s#git@bitbucket.org:#http://bitbucket.org/# -- use the public repos and you won't have problem
* I still can't clone bugparty! It says it is private.
  * Change it to bugparty-pub instead
* I need Elasticsearch globally visible
  * Edit the elasticsearch.yml file and add
    network.bind_host: "0.0.0.0"
  * docker rm es (after you devenv'd down everything)
* I use SE Linux
  * Be sure to export your homedirectory:
    sudo chcon -Rt svirt_sandbox_file_t /yourhomedir
* Do you have fastrep, bug-deduplication, docker and bugparty all checked out in your homedir?